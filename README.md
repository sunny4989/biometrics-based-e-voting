# Biometrics based e-voting System

It is a web based solution that can be used for conducting elections, survey, polls, etc.. With human biometrics using web as platform, it not only provides a highly secure authentication method, but also a anytime, anywhere solution which is highly customizable and simple to use.

Major Components
================

## Fingerprint authentication api

It is an independent API responsible for fingerprint matching (1:1 or 1:N) 

## Global Profile Manager

## Survey Software